import javax.lang.model.type.ArrayType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

/**
 * Created by Igor Soroka on 13.05.2016.
 */
public class Room {
    private int id;
    private String name;
    private int north;
    private int south;
    private int east;
    private int west;
    private ArrayList<String> listOfObjects;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getNorth() {
        return north;
    }
    public void setNorth(int north) {
        this.north = north;
    }

    public int getSouth() {
        return south;
    }
    public void setSouth(int south) {
        this.south = south;
    }

    public int getEast() {
        return east;
    }
    public void setEast(int east) {
        this.east = east;
    }


    public int getWest() {
        return west;
    }
    public void setWest(int west) {
        this.west = west;
    }

    public ArrayList<String> getListOfObjects() {
        return listOfObjects;
    }
    public void setListOfObjects(ArrayList<String> listOfObjects) {
        this.listOfObjects = listOfObjects;
    }

    public Room(int id, String name, int north, int south, int east, int west, ArrayList<String> listOfObjects) {
        this.id = id;
        this.name = name;
        this.north = north;
        this.south = south;
        this.east = east;
        this.west = west;
        this.listOfObjects = listOfObjects;
    }

    @Override
    public String toString() {
            return  "ID: " + id + "     " +
                    "Room: " + name + "        " +
                    "Items: " + listOfObjects;
    }
}
