import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by Igor Soroka on 13.05.2016.
 */
public class XMLParser {

    private ArrayList<Room> rooms = new ArrayList<>();
    public ArrayList<Room> getRooms() {
        return rooms;
    }
    public XMLParser(String xml) throws FileNotFoundException {
        //In our case I used FileInputStream to read byte stream instead of character stream. During xml validation it was found
        //that there some inappropriate symbols before the beginning of xml.
        FileInputStream fis = new FileInputStream(xml);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse(new InputSource(fis));
            doc.getDocumentElement().normalize();
            NodeList listOfRooms = doc.getElementsByTagName("room");

            for (int i = 0; i < listOfRooms.getLength(); i++) {
                //Creating null room to add parameters later in dependency of their presence. In direction "0" means there is no way further
                Room room = new Room(0, null, 0, 0, 0, 0, null);
                Node firstRoomNode = listOfRooms.item(i);
                if(firstRoomNode.getNodeType() == Node.ELEMENT_NODE){
                    Element firstRoomElement = (Element)firstRoomNode;

                    int id = Integer.parseInt(firstRoomElement.getAttribute("id"));
                    room.setId(id);
                    String name = firstRoomElement.getAttribute("name");
                    room.setName(name);

                    String north = firstRoomElement.getAttribute("north");
                    if (!north.equals(""))
                        room.setNorth(Integer.parseInt(north));
                    String south = firstRoomElement.getAttribute("south");
                    if (!south.equals(""))
                        room.setSouth(Integer.parseInt(south));
                    String east = firstRoomElement.getAttribute("east");
                    if (!east.equals(""))
                        room.setEast(Integer.parseInt(east));
                    String west = firstRoomElement.getAttribute("west");
                    if (!west.equals(""))
                        room.setWest(Integer.parseInt(west));

                    NodeList nodesObjects = firstRoomElement.getElementsByTagName("object");
                    ArrayList<String> objects = new ArrayList<>();
                    if (nodesObjects.getLength() != 0) {
                        for (int j = 0; j < nodesObjects.getLength(); j++) {
                            Node firstObjectNode = nodesObjects.item(j);
                            if(firstRoomNode.getNodeType() == Node.ELEMENT_NODE){
                                Element firstObjectElement = (Element)firstObjectNode;
                                String object = firstObjectElement.getAttribute("name");
                                objects.add(object);
                            }
                        }
                        room.setListOfObjects(objects);
                    }
                    rooms.add(room);
                }
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
