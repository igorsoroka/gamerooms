import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Igor Soroka on 14.05.2016.
 */
public class TxtReader {
    private int position;
    private ArrayList<String> objectsOfInterest = new ArrayList<>();


    public ArrayList<String> getObjectsOfInterest() {
        return objectsOfInterest;
    }

    public void setObjectsOfInterest(ArrayList<String> objectsOfInterest) {
        this.objectsOfInterest = objectsOfInterest;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void openFile (String file)
    {
        String line = null;
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null)
            {
                try {
                    setPosition(Integer.parseInt(line));
                }
                catch (NumberFormatException e) {
                    objectsOfInterest.add(line);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
