import com.sun.corba.se.impl.orbutil.graph.Graph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;
import java.util.logging.*;

/**
 * Created by Igor Soroka on 14.05.2016.
 */
public class SearchingThruRooms {
    Logger logger = Logger.getLogger("");
    FileHandler handler = null;

    private int vNum = 8;
    //visited or not
    private boolean[] visited = new boolean[vNum];
    //all connections between rooms
    private int [][] graph = {
            {0, 1},       //id 1
            {0, 3, 2, 6}, //id 2
            {1, 4, 5},    //id 3
            {1},          //id 4
            {2, 7},       //id 5
            {2},          //id 6
            {1, 7},       //id 7
            {4, 6}        //id 8
    };

    //This is a non-recursive implementation of Depth First Search Algorithm.
    public void searchAlgorithm(ArrayList<Room> roomList, int startRoom, ArrayList<String> objectsForSearch) {
        loggerCreation();
        //counter of found objects
        int counter = 0;
        //Sorting objects in alphabetical order to correspond for array of objects in the room
        Collections.sort(objectsForSearch);
        //Stack for adding there rooms
        Stack<Integer> s = new Stack<>();
        s.push(startRoom-1);
        int[] prevVecStr = new int[4];
        int[] currentVecStr;
        int currentRoom;

        while (!s.isEmpty())
        {
            if (counter == objectsForSearch.size())
            {
                System.out.println("Everything Found! ! !");
                break;
            }

            int v = s.peek(); //top element of the stack
            s.pop(); //deletes from the stack
            for (int i = 0; i < graph[v].length; ++i)
            {
                currentVecStr = graph[graph[v][i]];
                //Checking rooms not visited yet
                if (!visited[graph[v][i]])
                {
                    //Two for loops for finding rooms in between
                    for (int j = 0; j < currentVecStr.length; j++) {
                        for (int k = 0; k < prevVecStr.length; k++) {
                            if (currentVecStr[j] == prevVecStr[k] && currentVecStr[j] != graph[v][i]) {
                                int btwn = currentVecStr[j];
                                System.out.println(roomList.get(btwn).toString());
                            }
                        }
                    }
                    currentRoom = graph[v][i];
                    System.out.println(roomList.get(currentRoom).toString());
                    s.push(graph[v][i]); //places the element on the top of the stack
                    visited[graph[v][i]] = true;
                    prevVecStr = graph[graph[v][i]];

                    try
                    {
                        for (int l=0; l < roomList.get(currentRoom).getListOfObjects().size(); l++)
                        {
                            for (int m = 0; m < objectsForSearch.size(); m++)
                            {
                                if (roomList.get(currentRoom).getListOfObjects().get(l).equals(objectsForSearch.get(m)))
                                {
                                    objectsForSearch.set(m, null);
                                    counter++;
                                }
                            }
                        }
                    }
                    catch (NullPointerException e)
                    {
                        handler.publish(new LogRecord(Level.INFO, "No objects in the room"));
                    }
                }
            }
        }
    }

    public void loggerCreation() {
        //Creating log file to find errors

        try {
            handler = new FileHandler("LogFile.log", 1048576, 1, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        handler.setLevel(Level.ALL);
        handler.setFormatter(new SimpleFormatter());
        logger.addHandler(handler);
    }
}

