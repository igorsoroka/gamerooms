import java.io.FileNotFoundException;
import java.util.Scanner;


/**
 * Created by Igor Soroka on 13.05.2016.
 */
public class MainClass {
    public static void main(String[] args) throws FileNotFoundException {
        //Relative paths to the files
        String xmlFile = ".\\Docs\\map.xml";
        XMLParser XMLParser = new XMLParser(xmlFile);

        while(true)
        {
            try {
            System.out.println("***********");
            System.out.println("Automatic search game through rooms for finding objects specified in files");
            System.out.println("Choose file to open:");
            System.out.println("1 - Open config1.txt");
            System.out.println("2 - Open config2.txt");
            System.out.println("0 - Stop this, please!");
                Scanner sc = new Scanner(System.in);
                int choice = sc.nextInt();
                if (choice == 0)
                {
                    break;
                }
                switch(choice)
                {
                    case 1:
                        String txtFile1 = ".\\Docs\\config1.txt";
                        TxtReader txtReader = new TxtReader();
                        txtReader.openFile(txtFile1);
                        SearchingThruRooms str = new SearchingThruRooms();
                        str.searchAlgorithm(XMLParser.getRooms(), txtReader.getPosition(), txtReader.getObjectsOfInterest());
                        break;
                    case 2:
                        String txtFile2 = ".\\Docs\\config2.txt";
                        TxtReader txtReader2 = new TxtReader();
                        txtReader2.openFile(txtFile2);
                        SearchingThruRooms str2 = new SearchingThruRooms();
                        str2.searchAlgorithm(XMLParser.getRooms(), txtReader2.getPosition(), txtReader2.getObjectsOfInterest());
                        break;
                    default:
                        System.out.println("Repeat the input:  1, 2, 0");
                        break;
                }
            }
            catch (Exception e)
            {
                System.out.println("Please repeat the input: 1, 2, 0");
            }
        }

    }
}
